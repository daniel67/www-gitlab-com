---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2020-08-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 140       | 10.94%          |
| Based in EMEA                               | 344       | 26.88%          |
| Based in LATAM                              | 20        | 1.56%           |
| Based in NORAM                              | 776       | 60.63%          |
| **Total Team Members**                      | **1,280** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 893       | 69.77%          |
| Women                                       | 387       | 30.23%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,280** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 157       | 67.97%          |
| Women in Management                         | 74        | 32.03%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **231**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 71        | 75.53%          |
| Women in Leadership                         | 23        | 24.47%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **94**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 437       | 81.23%          |
| Women in Development                        | 101       | 18.77%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **538**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.28%           |
| Asian                                       | 47        | 6.46%           |
| Black or African American                   | 19        | 2.61%           |
| Hispanic or Latino                          | 40        | 5.50%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 30        | 4.13%           |
| White                                       | 447       | 61.49%          |
| Unreported                                  | 142       | 19.53%          |
| **Total Team Members**                      | **727**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 16        | 7.58%           |
| Black or African American                   | 4         | 1.90%           |
| Hispanic or Latino                          | 8         | 3.79%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 9         | 4.27%           |
| White                                       | 139       | 65.88%          |
| Unreported                                  | 35        | 16.59%          |
| **Total Team Members**                      | **211**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 6.04%           |
| Black or African American                   | 2         | 1.34%           |
| Hispanic or Latino                          | 4         | 2.68%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 6         | 4.03%           |
| White                                       | 94        | 63.09%          |
| Unreported                                  | 34        | 22.82%          |
| **Total Team Members**                      | **149**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 12.00%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 5.33%           |
| White                                       | 47        | 62.67%          |
| Unreported                                  | 15        | 20.00%          |
| **Total Team Members**                      | **75**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.16%           |
| Asian                                       | 116       | 9.06%           |
| Black or African American                   | 32        | 2.50%           |
| Hispanic or Latino                          | 66        | 5.16%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.08%           |
| Two or More Races                           | 38        | 2.97%           |
| White                                       | 722       | 56.41%          |
| Unreported                                  | 303       | 23.67%          |
| **Total Team Members**                      | **1,280** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 54        | 10.04%          |
| Black or African American                   | 9         | 1.67%           |
| Hispanic or Latino                          | 26        | 4.83%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.19%           |
| Two or More Races                           | 15        | 2.79%           |
| White                                       | 307       | 57.06%          |
| Unreported                                  | 126       | 23.42%          |
| **Total Team Members**                      | **538**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 14        | 6.06%           |
| Black or African American                   | 3         | 1.30%           |
| Hispanic or Latino                          | 6         | 2.60%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 6         | 2.60%           |
| White                                       | 146       | 63.20%          |
| Unreported                                  | 56        | 24.24%          |
| **Total Team Members**                      | **231**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 10.64%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.06%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 4.26%           |
| White                                       | 56        | 59.57%          |
| Unreported                                  | 23        | 24.47%          |
| **Total Team Members**                      | **94**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 17        | 1.33%           |
| 25-29                                       | 214       | 16.72%          |
| 30-34                                       | 350       | 27.34%          |
| 35-39                                       | 293       | 22.89%          |
| 40-49                                       | 278       | 21.72%          |
| 50-59                                       | 113       | 8.83%           |
| 60+                                         | 15        | 1.17%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,280** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
